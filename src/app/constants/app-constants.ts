export const MENU = [
  {
    menuName: 'Home',
    link: 'home',
    menuIcon: 'home'
  },
  {
    menuName: 'Liste des produits',
    link: 'list-produits',
    menuIcon: 'list'
  }
];

export const MENU_HOME = ['Home Furnishings', 'Accesoires', 'Sports', 'Clothing Wear'];

export const SLIDER_HOME_ITEMS = [
  {
    itemname: 'Chaise',
    link: 'assets/images/chaise.jpg',
    id: 0
  },
  {
    itemname: 'Lampe',
    link: 'assets/images/lampe.jpg',
    id: 1
  }
];

export const LIMITED_EDITION = [
  {
    version: '1.0',
    editionName: 'White clock',
  }
];

export const LIST_PRODUITS = [
  {
    id: '1',
    name: 'Sticks',
    link: 'assets/images/sticks.jpg',
    picklesuite: 'John Doelson',
    price: 155.00,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut malesuada porta turpis, sit amet consequat sapien. Aenean dignissim dolor ut finibus tincidunt.',
    origine: 'Espagne'
  },
  {
    id: '2',
    name: 'Cup',
    link: 'assets/images/cup.jpg',
    picklesuite: 'Ahmed Baddaoui',
    price: 255.00,
    description: 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut sodales euismod metus, et posuere ante. Cras finibus ante in ante accumsan..',
    origine: 'Algerie'
  },
  {
    id: '3',
    name: 'Red lampe',
    link: 'assets/images/lampe-red.jpg',
    picklesuite: 'Eric Valois',
    price: 155.00,
    description: 'Praesent quis dui vitae magna elementum suscipit vitae ac sapien. Proin suscipit quam eget mauris viverra, ut vulputate nunc commodo. Morbi id sollicitudin lacus.',
    origine: 'France'
  },
  {
    id: '4',
    name: 'Steal',
    link: 'assets/images/steal.jpg',
    picklesuite: 'Marie Dupuis',
    price: 155.00,
    description: 'Maecenas suscipit erat ac ante aliquet gravida. Vestibulum dolor sapien, rhoncus in risus sit amet, commodo fermentum mauris. Proin sed ultrices augue. Maecenas cursus mattis enim.',
    origine: 'France'
  },
  {
    id: '5',
    name: 'Heater',
    link: 'assets/images/heater.jpg',
    picklesuite: 'John Doelson',
    price: 255.00,
    description: 'In hac habitasse platea dictumst. Cras at diam sit amet eros mattis elementum sed non odio. Integer quis lectus et eros tempus accumsan. Sed eget tellus nec lacus egestas accumsan. Mauris fermentum fermentum dignissim.',
    origine: 'Kenya'
  },
  {
    id: '6',
    name: 'Wood',
    link: 'assets/images/wood.jpg',
    picklesuite: 'John Doelson',
    price: 155.00,
    description: 'Sed nec dui quis urna mattis porttitor. Pellentesque ligula metus, hendrerit vitae tincidunt non, faucibus quis dui. Sed sagittis elit quam, sit amet mattis purus faucibus sit amet.',
    origine: 'Algérie'
  },
  {
    id: '7',
    name: 'Weight',
    link: 'assets/images/weight.jpg',
    picklesuite: 'Lebron James',
    price: 255.00,
    description: 'Proin mollis, velit quis maximus finibus, leo erat aliquet est, a rhoncus nibh risus a elit. Donec tellus tortor, tincidunt vel enim ut, vehicula laoreet dolor. Aenean semper ex vitae mollis sagittis.',
    origine: 'Mongolie'
  },
  {
    id: '8',
    name: 'Latern',
    link: 'assets/images/latern.jpg',
    picklesuite: 'Princess Lea',
    price: 255.00,
    description: 'Nulla sit amet tellus eu libero placerat volutpat. Proin rhoncus ipsum enim, non blandit nisi ornare iaculis. Cras tortor arcu, tincidunt ac ornare porttitor, consectetur sed neque.',
    origine: 'Haiti'

  }
];
