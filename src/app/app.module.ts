import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Imports
import { PreloadAllModules, RouterModule } from '@angular/router';

import { MaterialModule } from './modules/material/material.module';
import { SharedModule } from './modules/shared/shared.module';
import { ModalProduitDetailComponent } from './modals/modal-produit-detail/modal-produit-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalProduitDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    SharedModule,
    RouterModule.forRoot([
      {
        path: '',
        loadChildren: './modules/global/global.module#GlobalModule'
      },
      {
        path: 'global',
        loadChildren: './modules/global/global.module#GlobalModule'
      }
    ])
  ],
  providers: [],
  entryComponents: [
    ModalProduitDetailComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
