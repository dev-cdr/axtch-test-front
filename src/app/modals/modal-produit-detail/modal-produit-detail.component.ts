import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-modal-produit-detail',
  templateUrl: './modal-produit-detail.component.html',
  styleUrls: ['./modal-produit-detail.component.scss']
})
export class ModalProduitDetailComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ModalProduitDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

}
