import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalProduitDetailComponent } from './modal-produit-detail.component';

describe('ProduitDetailComponent', () => {
  let component: ModalProduitDetailComponent;
  let fixture: ComponentFixture<ModalProduitDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalProduitDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProduitDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
