export interface IProduit {
  id?: number;
  name?: string;
	link?: string;
	picklesuite?: string;
	price?: number;
	description?: string;
	origine?: string;
}

export class Produit implements IProduit {
  constructor(
		public id?: number,
		public name?: string,
		public picklesuite?: string,
		public price?: number,
		public description?: string,
		public origine?: string,
	) {}
}
