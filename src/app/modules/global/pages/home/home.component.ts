import { Component, OnInit } from '@angular/core';
import { MENU_HOME, SLIDER_HOME_ITEMS, LIMITED_EDITION } from '../../../../constants/app-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public menuHomes = MENU_HOME;
  public sliderHomeItems = SLIDER_HOME_ITEMS;
  public limitedEdition = LIMITED_EDITION[0];

  public indexClass = 0;

  constructor() { }

  ngOnInit() {
  }

  /**
   * @param index 
   * Method to set some class in the template
   */
  getClass(index: number) {
    this.indexClass = index;
  }

}
