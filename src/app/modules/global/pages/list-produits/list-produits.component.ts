import { Component, OnInit } from '@angular/core';
import { LIST_PRODUITS } from '../../../../constants/app-constants';
import { IProduit } from '../../../../models/produit.model';
import { PopinsService } from '../../../../services/popins.service';

@Component({
  selector: 'app-list-produits',
  templateUrl: './list-produits.component.html',
  styleUrls: ['./list-produits.component.scss']
})
export class ListProduitsComponent implements OnInit {
  public listProduits = LIST_PRODUITS;
  public pageTitle = 'List produits';
  constructor(
    private popinsService: PopinsService
  ) { }

  ngOnInit() {
  }

  /**
   * @param produit
   * Method for opening the 'detail' of a 'produit'
   */
  openPopin(produit: IProduit): void {
    this.popinsService.openProduitDetail(produit);
    console.log(produit);
  }

}
