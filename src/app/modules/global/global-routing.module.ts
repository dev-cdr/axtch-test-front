import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Imports
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { PopinComponent } from './pages/popin/popin.component';
import { ListProduitsComponent } from './pages/list-produits/list-produits.component';

const routes: Routes = [
    {
      path: '',
      component: LayoutComponent,
      children: [
          {
              path: '',
              component: HomeComponent,
          },
          {
            path: 'popin',
            component: PopinComponent,
          },
          {
            path: 'list-produits',
            component: ListProduitsComponent,
          }
      ]
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlobalRoutingModule { }
