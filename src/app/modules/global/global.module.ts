import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlobalRoutingModule } from './global-routing.module';

// Imports
import { SharedModule } from '../shared/shared.module';

import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { PopinComponent } from './pages/popin/popin.component';
import { ListProduitsComponent } from './pages/list-produits/list-produits.component';
import { AccesoiresComponent } from './components/accesoires/accesoires.component';
import { SportsComponent } from './components/sports/sports.component';
import { ClothingWearComponent } from './components/clothing-wear/clothing-wear.component';

@NgModule({
  declarations: [
    LayoutComponent,
    HomeComponent,
    CarouselComponent,
    PopinComponent,
    ListProduitsComponent,
    AccesoiresComponent,
    SportsComponent,
    ClothingWearComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GlobalRoutingModule
  ],
  bootstrap: [LayoutComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class GlobalModule { }
