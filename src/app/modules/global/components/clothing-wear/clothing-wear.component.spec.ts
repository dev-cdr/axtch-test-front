import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClothingWearComponent } from './clothing-wear.component';

describe('ClothingWearComponent', () => {
  let component: ClothingWearComponent;
  let fixture: ComponentFixture<ClothingWearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClothingWearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClothingWearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
