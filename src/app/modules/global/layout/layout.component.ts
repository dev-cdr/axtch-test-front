import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  template: `
  <div class="view globale">
      <router-outlet></router-outlet>
  </div>
`,
styles: [``]
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
