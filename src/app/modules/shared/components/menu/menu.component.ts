import { Component, OnInit } from '@angular/core';
import { MENU } from '../../../../constants/app-constants';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {
  public menus = MENU;

  constructor(
    private router: Router
  ) {    }

  ngOnInit() {    }

  /**
   * @param link 
   * Method for navigation though the menu btn
   */
  navigate(link: string) {
    link === 'home' ? this.router.navigateByUrl('') : this.router.navigateByUrl(link);
  }
}
