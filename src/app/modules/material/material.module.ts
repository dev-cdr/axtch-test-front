import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatCardModule,
    MatDialogModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatAutocompleteModule,
    MAT_CHIPS_DEFAULT_OPTIONS,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatButtonModule,
    MatSnackBarModule,
    MatBottomSheetModule,
    MatProgressBarModule,
    MatMenuModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule
} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

const declarations = [
    MatCheckboxModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule,
    MatDialogModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatButtonModule,
    MatSnackBarModule,
    MatBottomSheetModule,
    MatProgressBarModule,
    MatMenuModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule
];

@NgModule({
    imports: [
        CommonModule,
        declarations,
    ],
    exports: [
        declarations,
    ],
    providers: [
        {
            provide: MAT_CHIPS_DEFAULT_OPTIONS,
            useValue: {
                separatorKeyCodes: [
                    ENTER,
                    COMMA,
                ]
            }
        }
    ]
})

export class MaterialModule {}
