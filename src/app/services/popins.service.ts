import { Injectable } from '@angular/core';
import { ModalProduitDetailComponent } from '../modals/modal-produit-detail/modal-produit-detail.component';
import { MatDialog } from '@angular/material';
import { IProduit } from '../models/produit.model';

@Injectable({
  providedIn: 'root'
})
export class PopinsService {

  constructor(
    private matDialog: MatDialog
  ) { }

  public openProduitDetail(produit: IProduit) {

    const profilDialog = this.matDialog.open(ModalProduitDetailComponent, {
      width: '90%',
      maxWidth: '500px',
      height: 'auto',
      maxHeight: '90%',
      disableClose: true,
      data: produit
    });

    const subscription = profilDialog.afterClosed().subscribe(res => {
      if (res) {
        console.log('modal data', res);
      }

      subscription.unsubscribe();
    });
  }
}
