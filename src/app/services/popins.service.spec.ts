import { TestBed } from '@angular/core/testing';

import { PopinsService } from './popins.service';

describe('PopinsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PopinsService = TestBed.get(PopinsService);
    expect(service).toBeTruthy();
  });
});
